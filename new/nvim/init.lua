
-----------
--aliases--
-----------

local bo = vim.bo
local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local map = vim.api.nvim_set_keymap
local o = vim.o
local wo = vim.wo

-----------
--options--
-----------
o.termguicolors = true
o.updatetime = 100
o.encoding = 'utf-8'
o.fileencoding = 'utf-8'
g.mapleader = ' '
o.cmdheight=2
o.shortmess = o.shortmess..'c'
o.laststatus = 2
o.wrap = false

--buffer

bo.expandtab = true
bo.shiftwidth = 4
bo.tabstop = 4
bo.undofile = true
bo.autoindent = true
bo.smartindent = true

--window

wo.cursorline = true
wo.number = true
wo.colorcolumn = '121'
wo.relativenumber = false
wo.signcolumn = 'yes'
cmd 'packadd paq-nvim'
--vim.cmd 'packadd paq-nvim'
--local paq = reuire'paq-nvim'.paq
--paq{'savq/paq-nvim', opt=true}

-----------
--plugins--
-----------

--auto install plugin manager

local install_path = fn.stdpath('data')..'/site/pack/paqs/opt/paq-nvim'

if fn.empty(fn.glob(install_path)) > 0 then
    cmd('!git clone --depth 1 https://github.com/savq/paq-nvim.git '..install_path)
end

cmd 'packadd paq-nvim'
local plug = require('paq-nvim').paq

--let the plugin manager manage itself

plug {'savq/paq-nvim', opt=true}

--list plugins to install
plug {'neoclide/coc.nvim'}
plug {'vim-airline/vim-airline'}
plug {'vim-airline/vim-airline-themes'}
plug {'airblade/vim-gitgutter'}
plug {'scrooloose/nerdtree'}
plug {'ctrlpvim/ctrlp.vim'}
plug {'nvim-treesitter/nvim-treesitter'}
plug {'tpope/vim-commentary'}

--themes
plug {'ayu-theme/ayu-vim'}
plug {'folke/tokyonight.nvim'}

--auto install and clean plugins
require('paq-nvim').install()
require('paq-nvim').clean()

-----------------
--plugin config--
-----------------

-- g.ayucolor = 'mirage'
cmd 'colorscheme tokyonight'

cmd 'autocmd FileType yaml,sql,javascript,typescriptreact,json setlocal ts=2 sts=2 sw=2 expandtab'

map('n', 'H', '^', {noremap = true})
map('n', 'L', '$', {noremap = true})
map('n', '^', 'H', {noremap = true})
map('n', '$', 'L', {noremap = true})
map('n', '<Up>', '<C-w>k', {noremap = true})
map('n', '<Down>', '<C-w>j', {noremap = true})
map('n', '<Left>', '<C-w>h', {noremap = true})
map('n', '<Right>', '<C-w>l', {noremap = true})

map('n', '<tab>', ':bnext<cr>', {noremap = true})
map('n', '<s-tab>', ':bprevious<cr>', {noremap = true})

map('n', '<C-B>', ':NERDTreeToggle<CR>', {noremap = true, silent = true})

cmd 'autocmd ColorScheme * hi Normal ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi EndOfBuffer ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi LineNr ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi NonText ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi SignColumn ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi GitGutterAdd ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi GitGutterChange ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi GitGutterDelete ctermbg=NONE guibg=NONE'
cmd 'autocmd ColorScheme * hi GitGutterChangeDelete ctermbg=NONE guibg=NONE'

require'nvim-treesitter.configs'.setup {
    highlight = {
        enable = true,
    }
}
---------
--ctrlp--
---------

g.ctrlp_working_path_mode = 'ra'
g.wildcardignore = '*/node_modules/*'

cmd 'inoremap <expr> <cr> pumvisible() ? "\\<C-y>" : "\\<C-g>u\\<CR>"'
cmd 'inoremap <expr> <Tab> pumvisible() ? "\\<C-n>" : "\\<Tab>"'
cmd 'inoremap <expr> <S-Tab> pumvisible() ? "\\<C-p>" : "\\<S-Tab>"'

cmd 'inoremap <silent><expr> <c-space> coc#refresh()'

