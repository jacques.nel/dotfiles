#!/usr/bin/env bash

sudo add-apt-repository -y ppa:neovim-ppa/unstable
sudo apt-get update

sudo apt install -y zsh neovim cmake build-essential

mkdir -p ~/.config/nvim/bundle
ln -sf ~/repos/jacques.nel/dotfiles/neovim/init.vim ~/.config/nvim/init.vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.config/nvim/bundle/Vundle.vim

pip3 install neovim

nvim -c PluginInstall -c quitall
pushd ~/.config/nvim/bundle/YouCompleteMe
python setup.py
popd

sudo chsh -s $(where zsh) jacques.nel
